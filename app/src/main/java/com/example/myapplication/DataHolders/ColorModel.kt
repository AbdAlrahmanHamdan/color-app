package com.example.myapplication.DataHolders

data class ColorModel (var colorName: String, var colorValue: String, var colorDescription: String, var ID: Any? = null)