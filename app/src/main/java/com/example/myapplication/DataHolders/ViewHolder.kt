package com.example.myapplication.DataHolders

import android.view.View
import com.example.myapplication.DataHolders.ItemType

class ViewHolder(_label: View?, _action: View?, _type: ItemType?) {
    var label: View? = _label
    var action: View? = _action
    var type: ItemType? = _type
}