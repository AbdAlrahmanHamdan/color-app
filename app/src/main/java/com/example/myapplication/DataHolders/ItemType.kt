package com.example.myapplication.DataHolders

enum class ItemType {
    Title, Name, ColorPicker, Buttons
}