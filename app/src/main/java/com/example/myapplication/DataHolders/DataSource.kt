package com.example.myapplication.DataHolders

data class DataSource (var label: String?, var value: String?, var type: ItemType?, var tag: Int? = null)