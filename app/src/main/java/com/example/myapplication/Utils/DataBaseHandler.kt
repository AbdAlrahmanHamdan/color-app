package com.example.myapplication.Utils

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.myapplication.DataHolders.ColorModel

class DataBaseHandler(context: Context) : SQLiteOpenHelper(context, "Color", null, 1) {
    override fun onCreate(p0: SQLiteDatabase?) {
        val createTable = "CREATE TABLE Color " +
                                 "(ColorID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                 "ColorName VARCHAR(256), " +
                                 "ColorDescription VARCHAR(256)," +
                                 "ColorValue VARCHAR(256))"
        p0?.execSQL(createTable)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {}

    fun insertData (colorModel: ColorModel) {
        val db = this.writableDatabase
        val contentValues = ContentValues().also {
            it.put("ColorName", colorModel.colorName)
            it.put("ColorDescription", colorModel.colorDescription)
            it.put("ColorValue", colorModel.colorValue)
        }
        val insertedID = db.insert("Color", null, contentValues)
        colorModel.ID = insertedID.toInt()
    }

    fun updateData (colorModel: ColorModel) {
        val db = this.writableDatabase
        val contentValues = ContentValues().also {
            it.put("ColorName", colorModel.colorName)
            it.put("ColorDescription", colorModel.colorDescription)
            it.put("ColorValue", colorModel.colorValue)
        }
        db.update("Color", contentValues, "ColorID="+colorModel.ID, null)
    }

    fun deleteData(ID: Int) {
        val db = this.writableDatabase
        db.delete("Color", "ColorID=$ID", null)
    }

    fun retrieveAllData () {
        val db = this.writableDatabase
        val cursor = db.rawQuery("SELECT * FROM Color", null)
        if (cursor.count != 0) {
            while (cursor.moveToNext()) {
                Utils.colorsInformation.add(ColorModel(cursor.getString(1), cursor.getString(3), cursor.getString(2), cursor.getInt(0)))
            }
        }
        else {
            insertData(ColorModel("Red", "#FF0000", "Red Red"))
        }
    }
}