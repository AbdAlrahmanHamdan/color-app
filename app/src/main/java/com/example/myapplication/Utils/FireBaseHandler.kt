package com.example.myapplication.Utils

import android.app.Application
import android.util.Log
import com.example.myapplication.DataHolders.ColorModel
import com.google.firebase.firestore.FirebaseFirestore

class FireBaseHandler : Application() {
    override fun onCreate() {
        super.onCreate()
    }

    fun insertData(colorModel: ColorModel){
        val db : FirebaseFirestore = FirebaseFirestore.getInstance()
        val valueToInsert: HashMap <String, String> = HashMap()
        valueToInsert["colorName"] = colorModel.colorName
        valueToInsert["colorValue"] = colorModel.colorValue
        valueToInsert["colorDescription"] = colorModel.colorDescription

        db?.collection("Colors")?.add(valueToInsert)
            ?.addOnSuccessListener {
                Log.i("Save Success", "Saved")
                colorModel.ID = it.id
            }
            ?.addOnFailureListener {
                Log.i("Failed", "Not Saved")
            }
    }

    fun deleteData(colorModel: ColorModel){
        val db : FirebaseFirestore = FirebaseFirestore.getInstance()
        db?.collection("Colors")?.document(colorModel.ID.toString())?.delete()
            ?.addOnSuccessListener{
                Log.i("Delete Success", "Deleted")
            }
            ?.addOnFailureListener {
                Log.i("Delete Failed", "Not Deleted")
            }
    }

    fun updateData(colorModel: ColorModel) {
        val db : FirebaseFirestore = FirebaseFirestore.getInstance()
        val valueToUpdate: HashMap <String, Any> = HashMap()
        valueToUpdate["colorName"] = colorModel.colorName
        valueToUpdate["colorValue"] = colorModel.colorValue
        valueToUpdate["colorDescription"] = colorModel.colorDescription
        db?.collection("Colors")?.document(colorModel.ID.toString())?.update(valueToUpdate)
    }

    fun retrieveAllData() {
        val db : FirebaseFirestore = FirebaseFirestore.getInstance()
        db?.collection("Colors")?.get()
            ?.addOnSuccessListener {
                it.documents.forEach{ documentSnapshot ->
                    Utils.colorsInformation.add(ColorModel(
                        documentSnapshot.data?.get("colorDescription").toString(),
                        documentSnapshot.data?.get("colorValue").toString(),
                        documentSnapshot.data?.get("colorDescription").toString(),
                        documentSnapshot.id))
                }
            }
    }
}