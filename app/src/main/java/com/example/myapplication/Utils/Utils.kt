package com.example.myapplication.Utils

import com.example.myapplication.DataHolders.ColorModel

class Utils {
    companion object {
        var colorsInformation: ArrayList<ColorModel> = ArrayList<ColorModel>()
        fun readColorData (colorsInformation: ArrayList<ColorModel>) {
            colorsInformation.add(
                ColorModel(
                    "Green",
                    "#00FF00",
                    "Seeing the colour green has been linked to more creative thinking—so greens are good options for home offices, art studios, etc."
                )
            )
            colorsInformation.add(
                ColorModel(
                    "Red",
                    "#FF0000",
                    "People seeing others in front of red backgrounds generally find those other individuals are more attractive than when they see them silhouetted against other colours, so reds are great for a bedroom wall. Having a red surface in view also gives us a burst of strength, so reds are good choices for home gym areas, etc. Seeing red has been linked to impaired analytical reasoning, though, making it a bad option for offices."
                )
            )
            colorsInformation.add(
                ColorModel(
                    "Violet",
                    "#EE82EE",
                    "People link a greyish violet with sophistication, so it can be a good selection for places where you’re trying to make the “right” impression."
                )
            )
            colorsInformation.add(
                ColorModel(
                    "Yellow",
                    "#FFFF00",
                    "Using yellow in a home can be problematic. Many people dislike the colour, so if you have a lot of yellow rooms in your home or a yellow front door, you may be advised to repaint to get the best price for your home should you sell. An exception: Many people use yellow in kitchens—with no negative sales repercussions. Yellow may be accepted in kitchens because warm colours stimulate our appetite."
                )
            )
            colorsInformation.add(
                ColorModel(
                    "Blue",
                    "#0000FF",
                    "People are more likely to tell you that blue is their favourite colour than any other shade. That makes it a safe choice. Seeing blue also brings thoughts of trustworthiness to mind; always a good thing."
                )
            )
            colorsInformation.add(
                ColorModel(
                    "White",
                    "#FFFFFF",
                    "People are more likely to tell you that blue is their favourite colour than any other shade. That makes it a safe choice. Seeing blue also brings thoughts of trustworthiness to mind; always a good thing."
                )
            )
        }
        var selectedItem: Int = 0
        var SELECTED_INDEX: String = "SELECTED_INDEX"
    }
}