package com.example.myapplication.Delegates

interface ColorFragmentDelegate {
    fun change(colorValue: String, colorDescription: String)
}