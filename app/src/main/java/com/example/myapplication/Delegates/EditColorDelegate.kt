package com.example.myapplication.Delegates

import com.example.myapplication.DataHolders.ColorModel

interface EditColorDelegate {
    fun save(colorModel: ColorModel)
    fun cancel()
}