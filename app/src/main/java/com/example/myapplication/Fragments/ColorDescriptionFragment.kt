package com.example.myapplication.Fragments

import android.graphics.Color.parseColor as colorParser
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import kotlinx.android.synthetic.main.fragment_color_description.*

class ColorDescriptionFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_color_description, container, false)
        return view
    }

    fun change(colorValue: String, colorDescription: String) {
        colorDescriptionText?.text = colorDescription
        this.view?.setBackgroundColor(colorParser(colorValue))
    }
}