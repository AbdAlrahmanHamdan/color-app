package com.example.myapplication.Fragments

import com.example.myapplication.Adapters.ColorAdapter
import com.example.myapplication.Utils.Utils
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.ListFragment
import com.example.myapplication.Delegates.ColorFragmentDelegate
import com.example.myapplication.R
import com.example.myapplication.Utils.FireBaseHandler

class ColorFragment : ListFragment() {
    private var adapter: ColorAdapter? = null
    private var colorFragmentDelegate: ColorFragmentDelegate? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_color, container, false)
        val fireBaseHandler = FireBaseHandler()
        if (savedInstanceState == null) {
            fireBaseHandler.retrieveAllData()
        }

        adapter = ColorAdapter(
            activity!!,
            R.layout.item_list_view,
            Utils.colorsInformation
        )
        listAdapter = adapter

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        listView.requestFocusFromTouch()
        listView.setSelection(Utils.selectedItem)

    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        Utils.selectedItem = position
        colorFragmentDelegate?.change(Utils.colorsInformation[position].colorValue, Utils.colorsInformation[position].colorDescription)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(Utils.SELECTED_INDEX, Utils.selectedItem)
        super.onSaveInstanceState(outState)
    }

    fun refreshData() {
        adapter?.notifyDataSetChanged()
    }

    fun performClick() {
        listView.performItemClick(listView.adapter.getView(
            Utils.selectedItem, null,
            listView.parent as ViewGroup?
        ), Utils.selectedItem, Utils.selectedItem.toLong())
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        colorFragmentDelegate = context as ColorFragmentDelegate
    }
}