package com.example.myapplication.Dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatDialogFragment
import com.example.myapplication.R
import kotlinx.android.synthetic.main.dialog_delete.view.*

class DeleteDialog(private val confirmPressed:()->Unit) : AppCompatDialogFragment(), View.OnClickListener {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val alertDialogBuilder : AlertDialog.Builder = AlertDialog.Builder(activity)
        val layoutInflater : LayoutInflater = activity!!.layoutInflater
        val view = layoutInflater.inflate(R.layout.dialog_delete, null)
        alertDialogBuilder.setView(view)
        view.deleteConfirmButton.also {
            it.setOnClickListener{
                confirmPressed()
                this.dismiss()
            }
        }
        view.cancelDeleteButton.also {
            it.setOnClickListener(this)
        }
        return alertDialogBuilder.create()
    }

    override fun onClick(p0: View?) {
        when(p0!!.id) {
            R.id.cancelDeleteButton -> {
                this.dismiss()
            }
        }
    }
}