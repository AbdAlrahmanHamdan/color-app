package com.example.myapplication.Activities

import com.example.myapplication.Adapters.UIAdapter
import com.example.myapplication.Delegates.EditColorDelegate
import com.example.myapplication.Utils.Utils
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.*
import com.example.myapplication.DataHolders.ColorModel
import com.example.myapplication.DataHolders.DataSource
import com.example.myapplication.DataHolders.ItemType
import com.example.myapplication.Utils.DataBaseHandler
import com.example.myapplication.Utils.FireBaseHandler
import kotlinx.android.synthetic.main.activity_edit_color.*


class AddNewColorActivity : AppCompatActivity(), EditColorDelegate {
    val dataBaseHandler = DataBaseHandler(this@AddNewColorActivity)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_color)
        val data : ArrayList<DataSource> = ArrayList()
        val listView: ListView = FormListView
        data.add(
            DataSource(
                "Add Color",
                null,
                ItemType.Title
            )
        )
        data.add(
            DataSource(
                "Color name: ",
                "",
                ItemType.Name,
                0
            )
        )
        data.add(
            DataSource(
                "Chose a color: ",
                "",
                ItemType.ColorPicker
            )
        )
        data.add(
            DataSource(
                "Color Description: ",
                "",
                ItemType.Name,
                1
            )
        )
        data.add(
            DataSource(
                "Save",
                "Cancel",
                ItemType.Buttons
            )
        )
        val adapter: UIAdapter? = UIAdapter(
            this,
            R.layout.activity_edit_color,
            data,
            ColorModel("", "", "")
        )

        listView.adapter = adapter
        adapter!!.editColorDelegate = this

    }

    override fun save(colorModel: ColorModel) {
        val fireBaseHandler = FireBaseHandler()
        fireBaseHandler.insertData(colorModel)
        Utils.colorsInformation.add(colorModel)
        val editColorIntent = Intent(applicationContext, MainActivity::class.java)
        setResult(Activity.RESULT_OK, editColorIntent)
        finish()
    }

    override fun cancel() {
        val editColorIntent = Intent(applicationContext, MainActivity::class.java)
        setResult(Activity.RESULT_OK, editColorIntent)
        finish()
    }
}