package com.example.myapplication.Activities

import com.example.myapplication.Delegates.ColorFragmentDelegate
import com.example.myapplication.Fragments.ColorDescriptionFragment
import com.example.myapplication.Fragments.ColorFragment
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.example.myapplication.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ColorFragmentDelegate, View.OnClickListener {
    var colorFragment: ColorFragment? = null
    var colorDescriptionFragment: ColorDescriptionFragment? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addNewColorButton.setOnClickListener(this)
        if (savedInstanceState == null)
            loadFragments()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun change(colorValue: String, colorDescription: String) {
        colorDescriptionFragment?.change(colorValue, colorDescription)
        colorFragment?.refreshData()
    }

    private fun loadFragments() {
        colorFragment = ColorFragment()
        colorDescriptionFragment = ColorDescriptionFragment()
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()

        colorFragment?.let {
            transaction.replace(R.id.Color, it, "colorFragment")
        }
        colorDescriptionFragment?.let {
            transaction.replace(R.id.ColorDescription, it, "colorDescriptionFragment")
        }

        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.addNewColorButton -> {
                val editColorIntent = Intent(this@MainActivity, AddNewColorActivity::class.java)
                startActivityForResult(editColorIntent, 1)
            }
        }
    }
}

