package com.example.myapplication.Activities

import com.example.myapplication.Adapters.UIAdapter
import com.example.myapplication.Delegates.EditColorDelegate
import com.example.myapplication.Utils.Utils
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.*
import com.example.myapplication.DataHolders.ColorModel
import com.example.myapplication.DataHolders.DataSource
import com.example.myapplication.DataHolders.ItemType
import com.example.myapplication.Utils.DataBaseHandler
import com.example.myapplication.Utils.FireBaseHandler
import kotlinx.android.synthetic.main.activity_edit_color.*


class EditColorActivity : AppCompatActivity(), EditColorDelegate {
    var position: Int? = null
    val dataBaseHandler = DataBaseHandler(this@EditColorActivity)
    override fun onCreate(savedInstanceState: Bundle?) {
        position = intent.getIntExtra("Position", 0)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_color)
        val data : ArrayList<DataSource> = ArrayList()
        val listView: ListView = FormListView
        addData(data)

        val adapter: UIAdapter? = UIAdapter(
            this,
            R.layout.activity_edit_color,
            data,
            Utils.colorsInformation[position!!]
        )
        listView.adapter = adapter
        adapter!!.editColorDelegate = this
    }

    override fun save(colorModel: ColorModel) {
        val fireBaseHandler = FireBaseHandler()
        Utils.colorsInformation[position!!] = colorModel
        fireBaseHandler.updateData(colorModel)
        val editColorIntent = Intent(applicationContext, MainActivity::class.java)
        setResult(Activity.RESULT_OK, editColorIntent)
        finish()
    }

    override fun cancel() {
        val editColorIntent = Intent(applicationContext, MainActivity::class.java)
        setResult(Activity.RESULT_OK, editColorIntent)
        finish()
    }

    private fun addData (data: ArrayList<DataSource>) {
        data.add(
            DataSource(
                "Edit Color",
                null,
                ItemType.Title
            )
        )
        data.add(
            DataSource(
                "Color name: ",
                Utils.colorsInformation[position!!].colorName,
                ItemType.Name,
                0
            )
        )
        data.add(
            DataSource(
                "Chose a color: ",
                Utils.colorsInformation[position!!].colorValue,
                ItemType.ColorPicker
            )
        )
        data.add(
            DataSource(
                "Color Description: ",
                Utils.colorsInformation[position!!].colorDescription,
                ItemType.Name,
                1
            )
        )
        data.add(
            DataSource(
                "Save",
                "Cancel",
                ItemType.Buttons
            )
        )
    }
}