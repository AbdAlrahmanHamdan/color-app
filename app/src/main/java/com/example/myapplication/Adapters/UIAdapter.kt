package com.example.myapplication.Adapters

import com.example.myapplication.Delegates.EditColorDelegate
import android.content.Context
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo.IME_FLAG_NO_ENTER_ACTION
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.myapplication.*
import com.example.myapplication.DataHolders.ColorModel
import com.example.myapplication.DataHolders.DataSource
import com.example.myapplication.DataHolders.ItemType
import com.example.myapplication.DataHolders.ViewHolder
import kotlinx.android.synthetic.main.item_buttons.view.*
import kotlinx.android.synthetic.main.item_color_name.view.*
import kotlinx.android.synthetic.main.item_color_picker.view.*
import kotlinx.android.synthetic.main.item_title.view.*
import yuku.ambilwarna.AmbilWarnaDialog
import yuku.ambilwarna.AmbilWarnaDialog.OnAmbilWarnaListener as OnAmbilWarnaListener1
import android.graphics.Color.parseColor as colorParser


class UIAdapter(var tempContext: Context, var resources: Int, var models: List<DataSource>, var colorObject: ColorModel) : ArrayAdapter<DataSource>(tempContext, resources, models), View.OnClickListener {
    var holder: ViewHolder? = null
    var retView: View? = null
    var colorValue: Int? = null
    var editColorDelegate: EditColorDelegate? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(tempContext)
        val view: View = layoutInflater.inflate(resources, null)
        when (models.get(position).type) {
            ItemType.Title -> {
                retView = layoutInflater.inflate(R.layout.item_title, null)
                holder = ViewHolder(
                    retView?.colorTitle,
                    null,
                    ItemType.Title
                )
                holder?.label = retView?.colorTitle as TextView
                holder?.label!!.colorTitle.text = models[position].label
            }
            ItemType.Name -> {
                retView = layoutInflater.inflate(R.layout.item_color_name, null)
                holder = ViewHolder(
                    retView?.colorNameLabel,
                    view.colorNameText,
                    ItemType.Name
                )
                holder?.label = retView?.colorNameLabel as TextView
                holder?.action = retView?.colorNameText as EditText
                holder?.label!!.colorNameLabel.text = models[position].label
                holder?.action!!.colorNameText.setText(models[position].value.toString())
                if (models[position].tag == 1) {
                    retView?.colorNameText?.isSingleLine = false
                    retView?.colorNameText?.imeOptions = IME_FLAG_NO_ENTER_ACTION
                }

                retView?.colorNameText!!.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                    }
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }
                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        if (models[position].tag == 1)
                            colorObject.colorDescription = s.toString()
                        else
                            colorObject.colorName = s.toString()
                    }
                })
            }
            ItemType.ColorPicker -> {
                retView = layoutInflater.inflate(R.layout.item_color_picker, null)
                holder = ViewHolder(
                    retView?.colorPickerLabel,
                    retView?.colorPickerButton,
                    ItemType.ColorPicker
                )
                holder?.label = retView?.colorPickerLabel as TextView
                holder?.action = retView?.colorPickerButton as Button
                holder?.label!!.colorPickerLabel.text = models[position].label
                if (models[position].value != "")
                    colorValue = colorParser(models[position].value)
                else
                    colorValue = Color.BLACK
                (retView?.colorPickerButton as Button).setOnClickListener(this)
            }
            ItemType.Buttons -> {
                retView = layoutInflater.inflate(R.layout.item_buttons, null)
                holder = ViewHolder(
                    retView?.saveChangesButton,
                    retView?.cancelChangesButton,
                    ItemType.ColorPicker
                )
                holder?.label = retView?.saveChangesButton as Button
                holder?.action = retView?.cancelChangesButton as Button
                holder?.label!!.saveChangesButton.text = models[position].label
                holder?.action!!.cancelChangesButton.text = models[position].value
                holder?.label!!.saveChangesButton.setOnClickListener(this)
                holder?.action!!.cancelChangesButton.setOnClickListener(this)
            }
        }
        return retView!!
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.colorPickerButton -> {
                openColorPicker()
            }

            R.id.saveChangesButton -> {
                colorObject.colorValue = '#' + Integer.toHexString(colorValue!!)
                editColorDelegate!!.save(colorObject)
                this.notifyDataSetChanged()
            }
            R.id.cancelChangesButton -> {
                editColorDelegate!!.cancel()
            }
        }
    }
    private fun openColorPicker () {
        AmbilWarnaDialog(context, colorValue!!, true,
            object : OnAmbilWarnaListener1 {
                override fun onCancel(dialog: AmbilWarnaDialog?) {}
                override fun onOk(dialog: AmbilWarnaDialog?, color: Int) {
                    colorValue = color
                }
            }).also {AmbilWarnaDialog-> AmbilWarnaDialog.show()}
    }
}

