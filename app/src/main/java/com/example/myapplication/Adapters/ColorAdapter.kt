package com.example.myapplication.Adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.DataHolders.ColorModel
import com.example.myapplication.Dialogs.DeleteDialog
import com.example.myapplication.Activities.EditColorActivity
import com.example.myapplication.Utils.DataBaseHandler
import com.example.myapplication.Utils.FireBaseHandler
import com.example.myapplication.Utils.Utils
import kotlinx.android.synthetic.main.item_list_view.view.*
import android.graphics.Color.parseColor as colorParser

class ColorAdapter(var tempContext: Context, var resources: Int, var models: List<ColorModel>) : ArrayAdapter<ColorModel>(tempContext, resources, models) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(tempContext)
        val view:View = layoutInflater.inflate(resources, null)

        loadUI(view, position)
        addClickListeners(view, position)

        return view
    }

    private fun openDialog (position: Int) {
        val deleteDialog = DeleteDialog {
            val fireBaseHandler = FireBaseHandler()
            fireBaseHandler.deleteData(Utils.colorsInformation[position])
            Utils.colorsInformation.removeAt(position)
            this.notifyDataSetChanged()
        }
        deleteDialog.show((context as AppCompatActivity).supportFragmentManager, "Delete Dialog")
    }

    private fun loadUI (view: View, position: Int) {
        val colorName = view.colorNameText
        val tempModel: ColorModel = models[position]
        colorName.text = tempModel.colorName
         view.circle.background.setColorFilter(colorParser(tempModel.colorValue), PorterDuff.Mode.SRC_IN)
    }

    private fun addClickListeners(view: View, position: Int) {
        view.editButton.setOnClickListener {
            val editColorIntent: Intent = Intent(context, EditColorActivity::class.java)
            editColorIntent.putExtra("Position", position)
            (context as Activity).startActivityForResult(editColorIntent, 1)
        }

        view.deleteButton.setOnClickListener{
            openDialog(position)
        }
    }
}